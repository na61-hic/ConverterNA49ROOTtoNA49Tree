#include "anar_fillMacro.h"
#include "T49Vertex.h"
#include "TMath.h"
#include "TH3D.h"
using namespace std;


Int_t anar_fillMacro(Int_t energy = 20, Int_t centSelection = 1, Int_t cutSelection = 1, Int_t inAcc = 0)
{
  gROOT->Reset();
  TStopwatch timer;
  timer.Reset();
  timer.Start();
  gSystem->Setenv("STAGE_POOL","na49_keep");
  Int_t maxEvt = 1000000;
  Float_t centCut = 3.5;
  TString useTight = "";
  if(0 == centSelection) centCut = 1.;
  if(0 == cutSelection) useTight = "_tight";

 ////
  Int_t isVenus = 0;
  TString venus="";
  TString accName = "";
  
  if(isVenus) 
  {
    venus="_VENUS";
    centCut = 1000; 
  }
  //// 
  Int_t bins[4];
  bins[0] = 2;  //charge
  bins[1] = 20; //Ptot
  bins[2] = 10; //pt
  bins[3] = 8; //Phi
  Char_t outpath1[256];
  TString outPath = "";
  if(1 == centSelection)
    sprintf(outpath1,"%sM_DataTree_%dGeV_mtpAllCharges_35Percent%s%s%s.root",outPath.Data(),energy,useTight.Data(),accName.Data(),venus.Data());
  else 
    if(0 == centSelection)
      sprintf(outpath1,"%sM_DataTree_%dGeV_mtpAllCharges_OnePercent%s%s%s.root",outPath.Data(),energy,useTight.Data(),accName.Data(),venus.Data()); 
    else
      {
	cout<<"Enter valid centSelection: 1 or 0"<<endl;
        return 0;
      }
  TFile *fout1 = new TFile(outpath1,"RECREATE");
  TTree* myTree  = new TTree("idenData","idenData");
  TTree* myTree_mult  = new TTree("mult","mult");
  TH1D  *histo         =    new TH1D("histo","histo",150,0.5,2.);
  TH1D  *histoAll      =    new TH1D("histoAll","histoAll",150,0.5,2.);
  TH1D  *histoRun      =    new TH1D("histoRun","histoRun",100,4500,5200);
  TH1D  *histoBin      =    new TH1D("histoBin","histoBin",150,0.5,2.);
  TH1D  *histoBinDist  =    new TH1D("histoBinDist","histoBinDist",1500,0.,1500.);
  Double_t dEdx, dEdxMTPC;
  Int_t myBin[4];
  Int_t myEventNum;
  Int_t multEvtVeto;
  Float_t momX, momY, momZ;
  Float_t bX, bY;
  Int_t iFlag;
  Float_t central;
  Int_t vertFlag;
  Float_t pchi2;
  myTree -> Branch("myBin",myBin,"myBin[4]/I");
  myTree -> Branch("myDeDx",&dEdxMTPC,"myDeDx/D");
  myTree -> Branch("evtNum",&myEventNum,"evtNum/I");

  myTree -> Branch("vFLag",&vertFlag,"vFlag/I");
  myTree -> Branch("pchi2",&pchi2,"pchi2/F");

  
  myTree -> Branch("momX",&momX,"momX");
  myTree -> Branch("iFlag",&iFlag,"iFlag");
  myTree -> Branch("momY",&momY,"momY");
  myTree -> Branch("momZ",&momZ,"momZ");
  myTree -> Branch("bX",&bX,"bX");
  myTree -> Branch("bY",&bY,"bY");
  myTree -> Branch("cent",&central,"cent");  
  myTree_mult -> Branch("multEvtVeto",&multEvtVeto,"multEvtVeto/I");
  
  Char_t outpath2[256];
  if(1 == centSelection)
    sprintf(outpath2,"%sM_Container_%dGeV_mtpAllCharges_35Percent%s%s%s.root",outPath.Data(),energy,useTight.Data(),accName.Data(),venus.Data());
  else 
    if(0 == centSelection)
      sprintf(outpath2,"%sM_Container_%dGeV_mtpAllCharges_OnePercent%s%s%s.root",outPath.Data(),energy,useTight.Data(),accName.Data(),venus.Data()); 
    else
      {
	cout<<"Enter valid centSelection: 1 or 0"<<endl;
        return 0;
      }
  TFile *fout2 = new TFile(outpath2,"RECREATE");
  T49Container *Contain   = new T49Container();
  T49Index     *FillIndex = new T49Index();	
  Double_t maxMom;
  Double_t minMom;  

  if(energy>=80)
    {
      maxMom = 120.;
      minMom = 2.;
      Contain -> Init(20, 2.,120., 10, 0., 2., 8);  
    }
  else
    {
      maxMom = 40.;
      minMom = 1.;
      Contain -> Init(20, 1., 40., 10, 0., 2., 8);
    }
  
  Contain -> SetTPC(2);
  Contain -> StoreNPoint();
    
  Char_t tmppath[256];
  sprintf(tmppath,"Mncs_tree_junk_%dGeV_mtpcl_FillList.root",energy);
  T49Run *run = new T49Run();
  run->SetVerbose(0);
  if(160 == energy)
    run -> SetEvetoCal(2);
  else
    run -> SetEvetoCal(1); 
  T49EventRoot *event;
  T49ParticleRoot *particle;
  TObjArray *Particles;
  T49Vertex *eVertex;

  Int_t EnInd = -1;
  if(energy == 20) EnInd = 0;
  if(energy == 30) EnInd = 1;
  if(energy == 40) EnInd = 2;
  if(energy == 80) EnInd = 3;
  if(energy == 160) EnInd = 4;
  Char_t name[5][256];

  sprintf(name[0],"20G+-20GeV-central-03A");
  sprintf(name[1],"30G+-30GeV-central-02J");
  sprintf(name[2],"1/4std+-40GeV-central-00W");
  sprintf(name[3],"1/2std+-80GeV-central-01E"); //3849
  if(!isVenus)
  sprintf(name[4],"std+-160GeV-central-00B"); //1389
  else
  sprintf(name[4],"std+-160GeV-central-00B"); 
  
  Int_t evtNum    = 0;
  multEvtVeto = 0;

  //while((run -> OpenNextRun(name[EnInd],kFALSE)))
  //  {
  TString inputDir = "/afs/cern.ch/user/r/rustamov/private/DSTS/";
  Char_t inputDirApp[255];
  sprintf(inputDirApp,"%dGeV/",energy);
  inputDir.Append(inputDirApp);
  cout<<"input directory is set to "<< inputDir.Data() << endl;
  Char_t DataBaseFile[255];
  TString files;
  Int_t fRunID;
  Int_t num;
  Char_t fFileName[255];
  Char_t fHostName[255];
  Char_t fRunType[255];
  const Char_t *DirName = gSystem->Getenv("ROOT49");
  sprintf(DataBaseFile,"%s/.root49dsts",DirName);
  FILE *fp = fopen(DataBaseFile,"r");
  Int_t countFiles = 0;
  
  while(fscanf(fp,"%d %d %s %s %s",&fRunID,&num, 
	       (char *) &fFileName,
	       (char *) &fHostName,
	       (char *) &fRunType) > 0)
    {
      if(multEvtVeto >= maxEvt) break;
      countFiles++;
      //cout<<"fileName == "<<fFileName<<endl;
      if (strcmp(fRunType,name[EnInd]) != 0) continue;
      TString opfile =  fFileName;
      opfile.ReplaceAll("/castor/cern.ch/na49/mini-dst/",inputDir);
      /*if(countFiles%10 == 0)*/cout<<"opening the file "<<opfile.Data()<<endl;
      
      if(opfile.Contains("t49run1457.3.root")) 
      {
       cout<<"scipping the file "<<opfile.Data()<<endl;
       continue;
      }
      
      if(opfile.Contains("t49run1457.7.root")) 
      {
       cout<<"scipping the file "<<opfile.Data()<<endl;
       continue;
      }
      
      
      
      if(!run -> Open((Char_t*)opfile.Data())) continue;
      //if(!run -> Open(num)) continue;
      //cout<<"test"<<endl; 
      run     -> SetRunType(name[EnInd]);
      run     -> SetRunNumber(num);   
      run     -> CheckEvetoCal();
      
    //while((run -> OpenNextRun(name[EnInd],kFALSE)))
    //{           
      while((event = (T49EventRoot *)run -> GetNextEvent()))
	{ 
          evtNum++;
	  if(evtNum%1000 == 0)
	    { 
	      cout<<"acc event: "<<multEvtVeto<<"; file: "<< event->GetNRun()<<fRunType<<endl;
	    }	 
          
          eVertex = (T49Vertex*) event -> GetFittedVertex(); 
          vertFlag = eVertex -> GetIflag();
          pchi2    = eVertex -> GetPchi2();          
          
         

	  histoRun -> Fill(event->GetNRun());
	  central = event -> GetCentrality();
	  if ( central >= centCut ) continue;
          multEvtVeto++;  
	  Particles = (TObjArray*)event -> GetPrimaryParticles();
	  TIter NextTrack(Particles); 
	  
	  ////////
	  
	  
	  
	  if(160 == energy)
	  {
	     if(pchi2 < 0) continue;          
          
	     Int_t multPart = 0;
	     while((particle = (T49ParticleRoot*)NextTrack.Next()))
	    {
	    multPart++;
	    }
	    if( multPart < 60 ) continue;
	    NextTrack.Reset();
	  }
	  
	  ////////
	  
	  while((particle = (T49ParticleRoot*)NextTrack.Next()))
	    {
	      dEdx     = particle -> GetTmeanCharge()/1000.;
	      dEdxMTPC = particle -> GetTmeanCharge(2)/1000.;
              momX = particle -> GetPx();
              momY = particle -> GetPy();
              momZ = particle -> GetPz();

              

	      bX = particle -> GetBx();
              bY = particle -> GetBy();
	      iFlag =  particle->GetIflag();
	      histoAll -> Fill(dEdxMTPC);
	      /////////////////////////////////////
             
	       /////////////////////////////////////
	       if(dEdx >= 1.8)                    continue;  
	       if(particle -> GetNPoint(2) <= 30) continue;
	       if(particle -> GetP() <= minMom || particle -> GetP() >= maxMom) continue;
	       if(particle -> GetPt() >= 2.)      continue; 
	       if(particle->GetNMaxPoint() <=0)   continue;
	       if(((Float_t)particle->GetNPoint()/(Float_t) particle->GetNMaxPoint()) <= 0.5) continue;
	       Contain -> GetIndex(particle,FillIndex);
	       myBin[0] = FillIndex -> GetChargeBin();
	       myBin[1] = FillIndex -> GetPtotBin();
	       myBin[2] = FillIndex -> GetPtBin();
	       myBin[3] = FillIndex -> GetPhiBin();
	       myEventNum = evtNum;    
              if( dEdxMTPC > 0.5 && dEdxMTPC < 2.)
		{  
		  if( Contain->FillContainer(particle) ) {myTree -> Fill();}
		}
	      if(dEdxMTPC <= 0.5 || dEdxMTPC >= 2.) continue;
	      histo -> Fill(dEdxMTPC);
	    }
	}
      run -> Close();
    }
  myTree_mult   -> Fill();
  fout1         -> cd();
  histo         -> Write();
  histoBin      -> Write();
  histoRun      -> Write();
  histoAll      -> Write();
  histoBinDist  -> Write();
  myTree        -> Write();  
  myTree_mult   -> Write();
  fout1 -> Close();
  fout2 -> cd();
  Contain      -> Write("cont",1);
  fout2         -> Close();
  timer.Stop();
  printf("Real time: %f\n",timer.RealTime());
  printf("CPU time: %f\n",timer.CpuTime());
  return 1;
}

int main(int argc, char *argv[])
{
  cout<<"***********************************************"<<endl;
  cout<<"***********************************************"<<endl;
  cout<<"******************** FLUCTUATION ANALYSIS  **********"<<endl;
  cout<<"***********************************************"<<endl;
  cout<<"***********************************************"<<endl;
  cout<<"      "<<endl;
  cout<<"      "<<endl;
  cout<<"      "<<endl;
  TROOT IdentityMethod("FLUCTUATION","FLUCTUATION ANALYSYS");
  switch (argc)
    {
    case 1:
      cout<<"areguments are: energy, cent. selection, cut selection "<<argv[0]<<endl;
      return 0;
      break;
    case 2:
      return anar_fillMacro(atoi(argv[1]));
      break;
    case 3:
      return anar_fillMacro(atoi(argv[1]), atoi(argv[2]));
      break;
    case 4:
      return anar_fillMacro(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
      break;
    case 5:
      return anar_fillMacro(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
      break;
    default:
      cout<<"areguments are: energy, cent. selection, cut selection "<<argv[0]<<endl;
      return 0;
    }
  return 1;
}

