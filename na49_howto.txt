example of the data file location:

nsls /castor/cern.ch/na49/mini-dst/00B

starting NA49 root:

export NA49_ROOT="/home/iselyuzh/work/na61/na49/T6149/install/"
source /opt/shine/start_shine
cmake -DCMAKE_INSTALL_PREFIX=${NA49_ROOT} -DDISABLE_DSPACK=on ../
source set49_local.sh

NA49 files and environment scripts for NA49
/afs/cern.ch/user/i/iselyuzh/na49/

example code from Anar:
/afs/cern.ch/user/r/rustamov/public/NA49Example/

Converted NA49-NA61 files from Tanja Susa
/afs/cern.ch/user/t/tsusa61/public/conversion


