#!/bin/bash

SCRIPT_NAME="$0"
NA49_INSTALL_PATH="$1"
[ "$NA49_INSTALL_PATH" == "" ] && echo "no destination directory provided... EXITING" && exit
# NA49_INSTALL_PATH="/afs/cern.ch/work/a/alzaytse/public/na49"
NA49_SOURCE_PATH="/afs/cern.ch/user/i/iselyuzh/public/na49"
ROOT_NA49_DIR="$NA49_INSTALL_PATH/ROOT_NA49"

echo "$SCRIPT_NAME:: Cleanup installation directory $NA49_INSTALL_PATH" 
[ -d "$NA49_INSTALL_PATH" ] && printf "$SCRIPT_NAME:: Clean target directory: $NA49_INSTALL_PATH? (yes/no) " && read delete_install_directory
[ "$delete_install_directory" == "yes" ] && rm -rf "$NA49_INSTALL_PATH"
[ -d "$NA49_INSTALL_PATH" ] && echo "script is broken - fix it!" && exit
mkdir "$NA49_INSTALL_PATH"

echo "$SCRIPT_NAME:: START: rsync (log file: $NA49_INSTALL_PATH/setup_NA49_rsync.log)"
rsync -av "$NA49_SOURCE_PATH"/ "$NA49_INSTALL_PATH"/ >& "$NA49_INSTALL_PATH/setup_NA49_rsync.log"
echo "$SCRIPT_NAME:: DONE: rsync of $NA49_INSTALL_PATH"

cd "$NA49_INSTALL_PATH"
cp set49_local_template.sh set49_local.sh
sed -i "s~REPLACE_HERE_NA49_INSTALL_PATH~$ROOT_NA49_DIR~g" set49_local.sh
source set49_local.sh
echo "$SCRIPT_NAME:: START: Clean up build directory"
rm -rf "$ROOT_NA49_DIR"/build/*
echo "$SCRIPT_NAME:: DONE: Clean up build directory"
cd "$ROOT_NA49_DIR"/build
echo "$SCRIPT_NAME:: START: cmake (log: $NA49_INSTALL_PATH/setup_NA49_cmake.log)"
cmake -DCMAKE_INSTALL_PREFIX=../ -DDISABLE_DSPACK=on ../ >& "$NA49_INSTALL_PATH/setup_NA49_cmake.log"
echo "$SCRIPT_NAME:: DONE: cmake"
echo "$SCRIPT_NAME:: START: Compiling NA49 ROOT (log: $NA49_INSTALL_PATH/setup_NA49_make_install.log)"
make install >& "$NA49_INSTALL_PATH/setup_NA49_make_install.log"
echo "$SCRIPT_NAME:: DONE: Compiling NA49 root"
echo "$SCRIPT_NAME:: START: Compiling example (log: $NA49_INSTALL_PATH/setup_NA49_example_make.log)"
cd "$NA49_INSTALL_PATH/NA49Example"
make clean >& "$NA49_INSTALL_PATH/setup_NA49_example_make_clean.log"
make >& "$NA49_INSTALL_PATH/setup_NA49_example_make.log"
echo "$SCRIPT_NAME:: DONE: Compiling example"
echo "$SCRIPT_NAME:: FINISH: NA49 root installation: $NA49_INSTALL_PATH"
echo "$SCRIPT_NAME:: FINISH: Example files: $NA49_INSTALL_PATH/NA49Example"

